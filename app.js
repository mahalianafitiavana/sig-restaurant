var map, currentLocation, markers;
markers = [];

function initialize() {
    document.getElementById('recherche').addEventListener('click', function() {
        recherche();
    });
    var mapOptions = {
        center: new google.maps.LatLng(-18.915504083349973, 47.52167339549441),
        zoom: 17
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    google.maps.event.addListener(map, 'click', function(event) {
        currentLocation = event.latLng;
        $('#exampleModal').modal('show');
    });
    find_all().then(list => {
        console.log(list);
        placeMarker(list);
        display_menu(list);
    }).catch(error => {
        console.error("Error fetching data: ", error);
    });
}
function find_all() {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (xhr.status == 200 && xhr.readyState == 4) {
                let list = JSON.parse(xhr.response);
                resolve(list);
            } else {
                reject("Error: " + xhr.status + " - " + xhr.statusText);
            }
        };
        xhr.onerror = function() {
            reject("Request failed");
        };
        xhr.open('POST', 'find.php', true);
        xhr.send();
    });
}
function find_by_menu(menu, km) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (xhr.status == 200 && xhr.readyState == 4) {
                let list = JSON.parse(xhr.response);
                resolve(list);
            } else {
                reject("Error: " + xhr.status + " - " + xhr.statusText);
            }
        };
        xhr.onerror = function() {
            reject("Request failed");
        };
        xhr.open('POST', 'find.php', true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var data = 'lat=' + encodeURI(currentLocation.lat()) +
            '&lng=' + encodeURI(currentLocation.lng()) +
            '&km=' + encodeURI(km) +
            '&menu=' + encodeURI(menu);
        console.log("Sending data:", data);
        xhr.send(data);
    });
}

function save (name, menu) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function (){
        if (xhr.status == 200 && xhr.readyState == 4) {
            // alert(xhr.responseText);
            // alert("Successfuly saved");
            document.getElementById('locationForm').reset();
        }else{
            alert('Error ');
        }
    };
    var data = 'latitude='+encodeURI( currentLocation.lat())+
    '&longitude='+encodeURI(currentLocation.lng())+
    '&name='+encodeURI(name)+
    '&menu='+encodeURI(menu);
    
    xhr.open('POST', 'save.php', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(data);
}
function get_all_menu (list) {
    let menuSet = new Set();
    list.forEach(restaurant => {

        let menus = restaurant.menu.split(",");

        menus.forEach(menu => menuSet.add(menu.trim()));
    });
    return Array.from(menuSet);
}
function display_menu (list){
    let menus = get_all_menu(list);
    // console.log(menus);
    let selectMenu =  document.getElementById('searchMenu');
    menus.forEach(menu => {
        const option = document.createElement('option');
        option.value = menu;
        option.textContent = menu;
        selectMenu.appendChild(option);
    });
}

function placeMarker(list) {
    clearMarkers();  // Nettoie les anciens marqueurs avant d'ajouter les nouveaux

    if (Array.isArray(list)) {
        for (const restaurant of list) {
            var coordinates = restaurant.coor.replace('POINT(', '').replace(')', '').split(' ');
            var longitude = parseFloat(coordinates[0]);
            var latitude = parseFloat(coordinates[1]);
            var location = new google.maps.LatLng(latitude, longitude);

            var markerTitle = "Restaurant: " + restaurant.name; 
            var menu = restaurant.menu.split(",");
            if (menu.length > 0) {
                markerTitle += "\nMenus:\n" + menu.join(", ");
            }

            var marker = new google.maps.Marker({
                position: location,
                map: map,
                title: markerTitle,
              
            });            
            markers.push(marker);  // Ajouter le marqueur au tableau des marqueurs
        }
    } else {
        console.error("Expected an array but got: ", list);
    }
}

function recherche() {
    var menu = document.getElementById('searchMenu').value;
    var km = document.getElementById('searchkm').value;
    console.log(menu, km);
    find_by_menu(menu, km).then(list => {
        console.log(list);
        placeMarker(list);
    }).catch(error => {
        console.error("Error fetching data: ", error);
    });
    document.getElementById('searchForm').reset();
}
function clearMarkers() {
    for (let marker of markers) {
        marker.setMap(null);
    }
    markers = [];
}
function submitForm() {
    var name = document.getElementById('name').value;
    var menu = document.getElementById('menu').value;

    $('#exampleModal').modal('hide');
    if (name.trim() !== "" && menu.trim() !== "") {
        save( name, menu);
    }
    find_all().then(list => {
        console.log(list);
        placeMarker(list);
        display_menu(list);
    }).catch(error => {
        console.error("Error fetching data: ", error);
    });

}
google.maps.event.addDomListener(window, 'load', initialize);
