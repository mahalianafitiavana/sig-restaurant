<?php
    function db_connect() {
        $user='postgres';
        $pass='postgres';
        $dsn='pgsql:host=localhost;port=5432;dbname=geo_db';    
        try {
            $bdd = new PDO($dsn, $user, $pass);
            return $bdd;
        } catch (PDOException $e) {
            print "Erreur ! : " . $e->getMessage();
            die();
        }
    }
    function save_resto ($name, $menu, $latitude, $longitude){
        try {
          $bdd = db_connect();
          $sql = "insert into restaurant (name, menu, coordinates) ";
          $sql .= "values (?, ?, st_geographyfromtext(?)) ";  // Added placeholders for name, menu and coordinates
          $stmt = $bdd->prepare($sql);
          $stmt->execute([$name, $menu, sprintf("POINT(%s %s)", $longitude, $latitude)]);  // Provide values in the same order as placeholders
          $bdd = null;
        } catch ( PDOException $e ) {
          throw $e;
        }
    }
    
    function find_all(){
        try {
            $bdd = db_connect();
            $sql = "select id, name, menu, ST_AsText(coordinates) as coor from restaurant ";
            $stmt = $bdd->prepare($sql);
            $stmt->execute();
            $liste = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $bdd = null;
            return $liste;
        } catch ( PDOException $e ) {
            throw $e;
        }
    }
    function select_by($menu, $km, $my_latitude, $my_longitude) {
        try {
            $bdd = db_connect();
            // Correct the SQL query to properly format the geography point and distance
            $sql = "SELECT id, name, menu, ST_AsText(coordinates) AS coor FROM restaurant 
                    WHERE ST_DWithin(coordinates, ST_GeogFromText(?), ?) 
                    AND menu LIKE ?";
            $stmt = $bdd->prepare($sql);
            // Prepare the parameters
            $point = sprintf("SRID=4326;POINT(%s %s)", $my_longitude, $my_latitude); // Note: Longitude comes first in POINT
            $distance = $km * 1000; // Convert km to meters if necessary
            $likeMenu = '%' . $menu . '%';
            // Execute with the correct parameters
            $stmt->execute([$point, $distance, $likeMenu]);
            $liste = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $bdd = null;
            return $liste;
        } catch (PDOException $e) {
            throw $e;
        }
    }
?>