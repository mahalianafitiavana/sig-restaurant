<?php
    require_once("function.php");
   
    try {
        if(isset($_POST['menu']) && isset($_POST['km']) && isset($_POST['lat']) && isset($_POST['lng'])){
            echo json_encode( select_by ($_POST['menu'], $_POST['km'], $_POST['lat'], $_POST['lng']));
        }else{
            echo json_encode( find_all ());
        }
    } catch (\Throwable $th) {
        echo $th->getMessage();
    }
    